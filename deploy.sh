IMAGE=docker.keith.sh/notes:$1

docker build . -t $IMAGE && \
    docker push $IMAGE && \
    kubectl --context do-nyc3-keithmackay-cluster -n notes \
    set image ds/notes server=$IMAGE

sleep 1
ATTEMPTS=0
ROLLOUT_STATUS_CMD="kubectl --context do-nyc3-keithmackay-cluster rollout status ds/notes -n notes"
until $ROLLOUT_STATUS_CMD || [ $ATTEMPTS -eq 60 ]; do
  $ROLLOUT_STATUS_CMD
  ATTEMPTS=$((ATTEMPTS + 1))
  sleep 5
done

ECHO "Successfully deployed" $1
