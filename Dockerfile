FROM node:alpine AS builder

WORKDIR /root

# RUN yarn global add ufnr

ADD ./package.json ./yarn.lock ./

RUN yarn

COPY ./ ./

RUN yarn build

FROM docker.keith.sh/static:latest

COPY --from=builder /root/public /files/
COPY --from=builder /root/build /files/
