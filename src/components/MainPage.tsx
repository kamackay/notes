import React from "react";
import { withRouter, RouteComponentProps } from "react-router-dom";
import { Input } from "antd";

interface Props extends RouteComponentProps<any> {}

interface State {
  text: string;
}

export default withRouter(
  class MainPage extends React.Component<Props, State> {
    constructor(props: Props) {
      super(props);
      this.state = {
        text: localStorage.getItem(`notes`) || ``,
      };
    }

    public render() {
      return (
        <div>
          <h1>Notes</h1>
          <Input.TextArea
            autoFocus
            defaultValue={this.state.text}
            placeholder="Enter Notes here..."
            style={{ width: "100%", height: "80vh" }}
            onChange={(data) => {
              localStorage.setItem(`notes`, data.target.value);
            }}
          />
        </div>
      );
    }
  }
);
